//
//  ConnectionManager.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 27/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "ConnectionManager.h"


@implementation ConnectionManager

+ (ConnectionManager *) sharedInstance
{
    static ConnectionManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ConnectionManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}



- (void) uploadImagewithHttpHeaders:(NSMutableDictionary*)headers withParameters:(NSMutableDictionary*)params withServiceName:(NSString*)serviceName withFilePath:(NSURL*)filePath withSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success withFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    
    /* if (![CommonFunctions isNetworkRechable])
     {
     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"NO INTERNET CONNECTION" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alertView show];
     
     [MBProgressHUD hideHUDForView:[APPDELEGATE window] animated:YES];
     
     return;
     }*/
    
    NSString *serviceUrl = serviceName;
    NSLog(@"Headers\n %@", headers);
    NSLog(@"Params\n %@", params);
    NSLog(@"ServiceUrl\n %@", serviceUrl);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    if (headers != nil)
    {
        NSArray *allHeaders = [headers allKeys];
        
        for (NSString *key in allHeaders)
        {
            [manager.requestSerializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
    }
    [manager POST:serviceUrl parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
        
        [formData appendPartWithFileURL:filePath name:@"image" error:nil];
        
    }
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         if (success != nil)
         {
             success(operation,responseObject);
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         
         if (failure != nil)
         {
             failure(operation,error);
         }
     }];
}

- (void) startRequestForMultipartFormDataWithHttpHeaders:(NSMutableDictionary*) headers withServiceName:(NSString*) serviceName withParameters:(NSMutableDictionary*) params withImageFile:(BOOL)isImagefile withImageInfo:(NSData*) imageInfo withSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success withFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSLog(@"Headers\n %@", headers);
    serviceName = serviceName;

    NSLog(@"ServiceUrl\n %@", serviceName);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    if (headers != nil)
    {
        NSArray *allHeaders = [headers allKeys];
        
        for (NSString *key in allHeaders)
        {

            [manager.requestSerializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
    }
    
    [manager POST:serviceName parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSString * mimeType;
        NSString * fileName;
        if(isImagefile){
            mimeType = @"image/png";
            fileName = @"image.png";
        }
        else{
            mimeType = @"video/mp4";
            fileName = @"video.mp4";
        }
        [formData appendPartWithFileData:imageInfo name:@"file" fileName:fileName mimeType:mimeType];
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        if (success != nil)
        {
            success(operation,responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        if (failure != nil)
        {
            failure(operation,error);
        }
        
    }];
}

- (void) startRequestWithHttpMethod:(kHttpMethodType) httpMethodType withHttpHeaders:(NSMutableDictionary*) headers withServiceName:(NSString*) serviceName withParameters:(NSMutableDictionary*) params withSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success withFailure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *serviceUrl = serviceName;
    NSLog(@"Headers\n %@", headers);
    NSLog(@"Params\n %@", params);
    NSLog(@"ServiceUrl\n %@", serviceUrl);

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:30];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    if (headers != nil)
    {
        NSArray *allHeaders = [headers allKeys];
        
        for (NSString *key in allHeaders)
        {
            [manager.requestSerializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
    }
    

    /*   [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString *(NSURLRequest *request, NSDictionary *parameters, NSError *__autoreleasing *error) {
     __block NSMutableString *query = [NSMutableString stringWithString:@""];
     
     NSError *err;
     NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
     NSMutableString *jsonString = [[NSMutableString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
     NSLog(@"jsonString %@", jsonString);
     query = jsonString;
     
     return query;
     }];*/

 /*   [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString *(NSURLRequest *request, NSDictionary *parameters, NSError *__autoreleasing *error) {
        __block NSMutableString *query = [NSMutableString stringWithString:@""];
        
        NSError *err;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
        NSMutableString *jsonString = [[NSMutableString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"jsonString %@", jsonString);
        query = jsonString;
        
        return query;
    }];*/

    
    switch (httpMethodType)
    {
        case kHttpMethodTypeGet:
        {
            [manager GET:serviceUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSLog(@"JSON: %@", responseObject);
                
                if (success != nil)
                {
                    success(operation,responseObject);
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                NSLog(@"Error: %@", error);
                
                if (failure != nil)
                {
                    failure(operation,error);
                }
            }];
        }
            break;
        case kHttpMethodTypePost:
        {
            [manager POST:serviceUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSLog(@"JSON: %@", responseObject);
                
                if (success != nil)
                {
                    success(operation,responseObject);
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                NSLog(@"Error:\n %@", error);
                if (failure != nil)
                {
                    failure(operation,error);
                }
            }];
        }
            break;
        case kHttpMethodTypeDelete:
        {
            [manager DELETE:serviceUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSLog(@"JSON: %@", responseObject);
                
                if (success != nil)
                {
                    success(operation,responseObject);
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                NSLog(@"Error: %@", error);
                
                if (failure != nil)
                {
                    failure(operation,error);
                }
                
            }];
            
        }
            break;
        case kHttpMethodTypePut:
        {
            [manager PUT:serviceUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSLog(@"JSON: %@", responseObject);
                
                if (success != nil)
                {
                    success(operation,responseObject);
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                NSLog(@"Error: %@", error);
                
                if (failure != nil)
                {
                    failure(operation,error);
                }
                
            }];
        }
            break;
            
        default:
            break;
    }
}

@end
