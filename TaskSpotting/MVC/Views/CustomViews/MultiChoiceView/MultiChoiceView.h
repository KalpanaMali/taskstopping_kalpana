//
//  MultiChoiceView.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol GetSelectedChoicesDelagate <NSObject> //this delegate is fired each time you clicked the cell

- (void)selectedChoices:(NSMutableArray *)arrChoices;

@end

@interface MultiChoiceView : UIView{
    
}
@property(nonatomic, assign)id<GetSelectedChoicesDelagate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UITableView *tblvTasks;
@property (strong, nonatomic) NSMutableArray *arrChoices;
@property (strong, nonatomic) NSMutableArray *arrSelectedChoices;

@end
