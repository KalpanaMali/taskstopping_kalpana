//
//  MultiChoiceView.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "MultiChoiceView.h"
#import "MultiChoiceCustomCell.h"
#import "QuestionOption.h"


@implementation MultiChoiceView

@synthesize arrChoices;
@synthesize arrSelectedChoices;
@synthesize delegate; //synthesize the delegate

 #pragma mark TableView Data Source
 
 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
 {
 // Return the number of sections.
 return 1;
 }
 
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
 {
 return [arrChoices count];
 }
 
 
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     
     MultiChoiceCustomCell *multiChoiceCell = [self.tblvTasks dequeueReusableCellWithIdentifier:@"MultiChoiceCustomCell"];
     
     if (!multiChoiceCell) {
         
         multiChoiceCell = (MultiChoiceCustomCell *)[[[NSBundle mainBundle] loadNibNamed:@"MultiChoiceCustomCell" owner:self options:nil] lastObject];
         
     }
     
     multiChoiceCell.selectionStyle = UITableViewCellSelectionStyleNone;
     QuestionOption *choices = [arrChoices objectAtIndex:indexPath.row];
     multiChoiceCell.lblRuleText.text = choices.value;
     if(choices.isSelected == YES){
         multiChoiceCell.imgvRuleIcon.image = [UIImage imageNamed:@"CheckedBox"];
         multiChoiceCell.isSelected = YES;
     }
     else{
         multiChoiceCell.imgvRuleIcon.image = [UIImage imageNamed:@"BlankCheckBox"];
         multiChoiceCell.isSelected = NO;

     }
     multiChoiceCell.textLabel.adjustsFontSizeToFitWidth = YES;
 
     return multiChoiceCell;
 }


- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    MultiChoiceCustomCell *cell = (MultiChoiceCustomCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    if(cell.isSelected == NO){
        cell.isSelected = YES;
        cell.imgvRuleIcon.image = [UIImage imageNamed:@"CheckedBox"];
        if(![arrSelectedChoices containsObject:[arrChoices objectAtIndex:indexPath.row]]){
            [arrSelectedChoices addObject:[arrChoices objectAtIndex:indexPath.row]];
        }

    }
    else{
        cell.isSelected = NO;
        
        cell.imgvRuleIcon.image = [UIImage imageNamed:@"BlankCheckBox"];

        if([arrSelectedChoices containsObject:[arrChoices objectAtIndex:indexPath.row]]){

            [arrSelectedChoices removeObject:[arrChoices objectAtIndex:indexPath.row]];
            QuestionOption *choices = [arrChoices objectAtIndex:indexPath.row];
            choices.isSelected = NO;
            [arrChoices replaceObjectAtIndex:indexPath.row withObject:choices];
        }

        
    }
//    for (int i = 0; i < [arrSelectedChoices count]; i++) {
//        QuestionOption *choice = [arrSelectedChoices objectAtIndex:i];
//        NSLog(@"arrSelectedChoices \n%@", choice.value);
//    }
    
    if ([delegate conformsToProtocol:@protocol(GetSelectedChoicesDelagate)] &&
        [delegate respondsToSelector:@selector(selectedChoices:)]) {
        // dispatch_sync(dispatch_get_main_queue(), ^{
        [delegate selectedChoices:arrSelectedChoices];
        // });
    }
}


@end
