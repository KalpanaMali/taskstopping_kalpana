//
//  MissionDetailCustomCell.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MissionDetailCustomCell : UITableViewCell{
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imgvCheckMark;

@property (weak, nonatomic) IBOutlet UIImageView *imgvRuleIcon;

@property (weak, nonatomic) IBOutlet UILabel *lblRuleText;


@end
