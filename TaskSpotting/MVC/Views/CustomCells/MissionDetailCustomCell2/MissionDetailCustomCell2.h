//
//  MissionDetailCustomCell2.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MissionDetailCustomCell2 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblQuestionCount;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskCount;
@property (weak, nonatomic) IBOutlet UILabel *lblPhotoCount;
@property (weak, nonatomic) IBOutlet UILabel *lblHours;
@property (weak, nonatomic) IBOutlet UILabel *lblDays;
@property (weak, nonatomic) IBOutlet UIImageView *imgvPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblAboutMission;
@property (weak, nonatomic) IBOutlet UIImageView *imgvQuestion;
@property (weak, nonatomic) IBOutlet UIImageView *imgvType;

@end
