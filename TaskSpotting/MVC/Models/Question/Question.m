//
//  Question.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "Question.h"

@implementation Question

@synthesize  default_value;
@synthesize  details;
@synthesize  icon;
@synthesize  task_id;
@synthesize  question;
@synthesize  skippable;
@synthesize  type;
@synthesize  type_id;
@synthesize  type_name;
@synthesize answerID;
@synthesize multipleAnswerID;
@synthesize queIndex;

-(id)init
{
    
    self=[super init];
    
    if (self)
    {
        default_value = @"";
        details = nil;
        icon = @"";
        task_id = 0;
        question = @"";
        skippable = @"";
        type = @"";
        type_id = @"";
        type_name = @"";
        answerID = 0;
        multipleAnswerID = nil;
        queIndex = 0;
    }
    return self;
}


+ (NSMutableArray*)initWithJSONData:(NSArray *) questionArr
{
    NSMutableArray *newArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < [questionArr count]; i++) {
        
        NSDictionary *resultDict = [questionArr objectAtIndex:i];
        
        Question *result = [[Question alloc] init];
        
        if ([resultDict objectForKey:@"default_value"] != nil) {
            result.default_value = [resultDict objectForKey:@"default_value"];
        }
        if ([resultDict objectForKey:@"details"] != nil) {
            id detail = [resultDict objectForKey:@"details"];
            if([detail count] == 0){
            }
            else{
                result.details = [TaskDetail initWithJSONData:[resultDict objectForKey:@"details"]];
            }

        }
        if ([resultDict objectForKey:@"icon"] != nil) {
            
            result.icon = [resultDict objectForKey:@"icon"];
        }
        
        if ([resultDict objectForKey:@"id"] != nil) {
            
            result.task_id = (NSNumber*)[resultDict objectForKey:@"id"];
        }

        if ([resultDict objectForKey:@"question"] != nil) {
            
            result.question = [resultDict objectForKey:@"question"];
        }

        if ([resultDict objectForKey:@"skippable"] != nil) {
            
            result.skippable = [resultDict objectForKey:@"skippable"];
        }

        if ([resultDict objectForKey:@"type"] != nil) {
            
            result.type = [resultDict objectForKey:@"type"];
        }

        if ([resultDict objectForKey:@"type_id"] != nil) {
            
            result.type_id = [resultDict objectForKey:@"type_id"];
        }

        if ([resultDict objectForKey:@"type_name"] != nil) {
            
            result.type_name = [resultDict objectForKey:@"type_name"];
        }

        
        [newArray addObject:result];
    }
    return  [NSMutableArray arrayWithArray:newArray];
}



@end
