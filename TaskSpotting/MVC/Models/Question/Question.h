//
//  Question.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TaskDetail.h"


@interface Question : NSObject


@property(nonatomic,retain) NSString    *default_value;
@property(nonatomic,retain) TaskDetail *details;
@property(nonatomic,retain) NSString    *icon;
@property(nonatomic,retain) NSNumber    *task_id;
@property(nonatomic,retain) NSString    *question;
@property(nonatomic,retain) NSString    *skippable;
@property(nonatomic,retain) NSString    *type;
@property(nonatomic,retain) NSString    *type_id;
@property(nonatomic,retain) NSString    *type_name;
@property(nonatomic,retain) NSNumber    *answerID;
@property(nonatomic,retain) NSMutableArray   *multipleAnswerID;
@property(nonatomic,assign) NSInteger    queIndex;


+ (NSMutableArray*)initWithJSONData:(NSArray *) questionArr;

@end
