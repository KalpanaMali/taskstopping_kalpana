//
//  TasksTypes.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TasksTypes : NSObject


@property(nonatomic,retain) NSString *count;
@property(nonatomic,retain) NSString *icon;
@property(nonatomic,retain) NSString *text;



+ (NSMutableArray*)initWithJSONData:(NSArray *) tasksArr;


@end
