//
//  TasksTypes.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "TasksTypes.h"

@implementation TasksTypes
@synthesize count;
@synthesize icon;
@synthesize text;


-(id)init
{
    
    self=[super init];
    
    if (self)
    {
        count                =@"";
        icon                =@"";
        text                =@"";
    }
    return self;
}


+ (NSMutableArray*)initWithJSONData:(NSArray *) tasksArr
{
    NSMutableArray *newArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < [tasksArr count]; i++) {
        
        NSDictionary *resultDict = [tasksArr objectAtIndex:i];
        
        TasksTypes *result = [[TasksTypes alloc] init];
        
        if ([resultDict objectForKey:@"count"] != nil) {
            result.count = [resultDict objectForKey:@"count"];
        }
        if ([resultDict objectForKey:@"icon"] != nil) {
            
            result.icon = [resultDict objectForKey:@"icon"];
        }
        if ([resultDict objectForKey:@"text"] != nil) {
            
            result.text = [resultDict objectForKey:@"text"];
        }
      
        
        [newArray addObject:result];
    }
    return  [NSMutableArray arrayWithArray:newArray];
}


@end
