//
//  NewPath.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewPath : NSObject


@property(nonatomic,retain) NSNumber *task_id;
@property(nonatomic,retain) NSNumber *answer_id;

+ (NSMutableArray*)initWithJSONData:(NSArray *) taskarr;

@end
