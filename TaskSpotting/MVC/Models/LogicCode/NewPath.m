//
//  NewPath.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "NewPath.h"

@implementation NewPath
@synthesize task_id;
@synthesize answer_id;


-(id)init
{
    
    self=[super init];
    
    if (self)
    {
        task_id                =0;
        answer_id                =0;

    }
    return self;
}


+ (NSMutableArray*)initWithJSONData:(NSArray *) taskarr
{
    NSMutableArray *newArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < [taskarr count]; i++) {
        
        NSDictionary *resultDict = [taskarr objectAtIndex:i];
        
        NewPath *result = [[NewPath alloc] init];
        
        if ([resultDict objectForKey:@"id"] != nil) {
            result.task_id = (NSNumber*)[resultDict objectForKey:@"id"];
        }
        [newArray addObject:result];
    }
    return  [NSMutableArray arrayWithArray:newArray];
}

@end
