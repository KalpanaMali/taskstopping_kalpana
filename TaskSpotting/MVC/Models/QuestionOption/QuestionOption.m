//
//  QuestionOption.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "QuestionOption.h"

@implementation QuestionOption


@synthesize option_id;
@synthesize next;
@synthesize value;
@synthesize isSelected;

-(id)init
{
    
    self=[super init];
    
    if (self)
    {
        option_id = 0;
        next = @"";
        value = @"";
        isSelected = NO;
    }
    return self;
}


+ (NSMutableArray*)initWithJSONData:(NSArray *) optionArr
{
    NSMutableArray *newArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < [optionArr count]; i++) {
        
        NSDictionary *resultDict = [optionArr objectAtIndex:i];
        
        QuestionOption *result = [[QuestionOption alloc] init];
        
        if ([resultDict objectForKey:@"id"] != nil) {
            result.option_id = (NSNumber*)[resultDict objectForKey:@"id"];
        }
        if ([resultDict objectForKey:@"next"] != nil) {
            
            result.next = [resultDict objectForKey:@"next"];
        }
        if ([resultDict objectForKey:@"value"] != nil) {
            
            result.value = [resultDict objectForKey:@"value"];
        }
      
        
        [newArray addObject:result];
    }
    return  [NSMutableArray arrayWithArray:newArray];
}


@end
