//
//  QuestionOption.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionOption : NSObject


@property(nonatomic,retain) NSNumber *option_id;
@property(nonatomic,retain) NSString *next;
@property(nonatomic,retain) NSString *value;
@property(nonatomic,assign) BOOL isSelected;


+ (NSMutableArray*)initWithJSONData:(NSArray *) tasksArr;


@end
