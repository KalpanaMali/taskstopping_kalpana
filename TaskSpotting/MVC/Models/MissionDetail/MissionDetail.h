//
//  MissionDetail.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MissionDetail : NSObject


@property(nonatomic,retain) NSString *application;
@property(nonatomic,retain) NSString *bonus_type;
@property(nonatomic,retain) NSString *bonus_value;
@property(nonatomic,retain) NSString *brand_id;
@property(nonatomic,retain) NSString *brand_name;
@property(nonatomic,retain) NSString *club_id;
@property(nonatomic,retain) NSString *country_id;
@property(nonatomic,retain) NSString *expire_date;
@property(nonatomic,retain) NSString *gender;
@property(nonatomic,retain) NSString *icons;
@property(nonatomic,retain) NSString *missionID;
@property(nonatomic,retain) NSString *instructions;
@property(nonatomic,retain) NSString *kit;
@property(nonatomic,retain) NSString *location_coordinates;
@property(nonatomic,retain) NSString *location_name;
@property(nonatomic,retain) NSString *location_type;
@property(nonatomic,retain) NSArray  *locations_list;
@property(nonatomic,retain) NSString *logo;
@property(nonatomic,retain) NSString *map_app;
@property(nonatomic,retain) NSString *map_icon;
@property(nonatomic,retain) NSString *mission_status;
@property(nonatomic,retain) NSString *mission_time;
@property(nonatomic,retain) NSString *name;
@property(nonatomic,retain) NSString *pin_color;
@property(nonatomic,retain) NSString *qualifying;
@property(nonatomic,retain) NSDictionary *reward;
@property(nonatomic,retain) NSString *reward_text;
@property(nonatomic,retain) NSString *rsvp;
@property(nonatomic,retain) NSMutableArray *rules;
@property(nonatomic,retain) NSString *share_msg;
@property(nonatomic,retain) NSString *social_rules;
@property(nonatomic,retain) NSString *start_enabled;
@property(nonatomic,retain) NSString *stealth_mode;
@property(nonatomic,retain) NSString *targetted_campaign_id;
@property(nonatomic,retain) NSString *tasks_count;
@property(nonatomic,retain) NSMutableArray *tasks_types;
@property(nonatomic,retain) NSDictionary *type;
@property(nonatomic,retain) NSString *user_mission_id;



+ (MissionDetail*)initWithJSONData:(NSDictionary *) resultsArr;



@end
