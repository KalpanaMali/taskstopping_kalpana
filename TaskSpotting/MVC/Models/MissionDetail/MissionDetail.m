//
//  MissionDetail.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "MissionDetail.h"
#import "MissionRules.h"
#import "TasksTypes.h"

@implementation MissionDetail

@synthesize application;
@synthesize bonus_type;
@synthesize bonus_value;
@synthesize brand_id;
@synthesize brand_name;
@synthesize club_id;
@synthesize country_id;
@synthesize expire_date;
@synthesize gender;
@synthesize icons;
@synthesize missionID;
@synthesize instructions;
@synthesize kit;
@synthesize location_coordinates;
@synthesize location_name;
@synthesize location_type;
@synthesize locations_list;
@synthesize logo;
@synthesize map_app;
@synthesize map_icon;
@synthesize mission_status;
@synthesize mission_time;
@synthesize name;
@synthesize pin_color;
@synthesize qualifying;
@synthesize reward;
@synthesize reward_text;
@synthesize rsvp;
@synthesize rules;
@synthesize share_msg;
@synthesize social_rules;
@synthesize start_enabled;
@synthesize stealth_mode;
@synthesize targetted_campaign_id;
@synthesize tasks_count;
@synthesize tasks_types;
@synthesize type;
@synthesize user_mission_id;

-(id)init
{
    
    self=[super init];
    
    if (self)
    {
        application = @"";
        bonus_type= @"";
        bonus_value= @"";
        brand_id= @"";
        brand_name= @"";
        club_id= @"";
        country_id= @"";
        expire_date= @"";
        gender= @"";
        icons= @"";
        missionID= @"";
        instructions= @"";
        kit= @"";
        location_coordinates= @"";
        location_name= @"";
        location_type= @"";
        locations_list= nil;
        logo= @"";
        map_app= @"";
        map_icon= @"";
        mission_status= @"";
        mission_time= @"";
        name= @"";
        pin_color= @"";
        qualifying= @"";
        reward= nil;
        reward_text= @"";
        rsvp= @"";
        rules= nil;
        share_msg= @"";
        social_rules= @"";
        start_enabled= @"";
        stealth_mode= @"";
        targetted_campaign_id= @"";
        tasks_count= @"";
        tasks_types= nil;
        type= nil;
        user_mission_id= @"";
    }
    return self;
}


+ (MissionDetail*)initWithJSONData:(NSDictionary *) resultDict
{
    
    MissionDetail *result = [[MissionDetail alloc] init];
    if ([resultDict objectForKey:@"message"] != nil)
    {
        NSDictionary *message = [resultDict objectForKey:@"message"];
        if ([message objectForKey:@"mission"] != nil)

        {
            NSDictionary *mission = [message objectForKey:@"mission"];

            if ([mission objectForKey:@"brand_id"] != nil) {
                result.brand_id = [mission objectForKey:@"brand_id"];
            }
            if ([mission objectForKey:@"brand_name"] != nil) {
                
                result.brand_name = [mission objectForKey:@"brand_name"];
            }
            if ([mission objectForKey:@"country_id"] != nil) {
                result.country_id = [mission objectForKey:@"country_id"];

            }
            if ([mission objectForKey:@"expire_date"] != nil) {
                
                result.expire_date = [mission objectForKey:@"expire_date"];
            }
            if ([mission objectForKey:@"gender"] != nil) {
                
                result.gender = [mission objectForKey:@"gender"];
            }
            
            if ([mission objectForKey:@"id"] != nil) {
                
                result.missionID = [mission objectForKey:@"id"];
            }
            if ([mission objectForKey:@"instructions"] != nil) {
                
                result.instructions = [mission objectForKey:@"instructions"];
            }
            if ([mission objectForKey:@"location_coordinates"] != nil) {
                
                result.location_coordinates = [mission objectForKey:@"location_coordinates"];
            }
            if ([mission objectForKey:@"location_name"] != nil) {
                
                result.location_name = [mission objectForKey:@"location_name"];
            }
            if ([mission objectForKey:@"logo"] != nil) {
                
                result.logo = [mission objectForKey:@"logo"];
            }
            if ([mission objectForKey:@"map_icon"] != nil) {
                
                result.map_icon = [mission objectForKey:@"map_icon"];
            }
            if ([mission objectForKey:@"mission_status"] != nil) {
                
                result.mission_status = [mission objectForKey:@"mission_status"];
            }
            if ([mission objectForKey:@"mission_time"] != nil) {
                
                result.mission_time = [mission objectForKey:@"mission_time"];
            }
            if ([mission objectForKey:@"name"] != nil) {
                
                result.name = [mission objectForKey:@"name"];
            }
            if ([mission objectForKey:@"pin_color"] != nil) {
                
                result.pin_color = [mission objectForKey:@"pin_color"];
            }
            if ([mission objectForKey:@"reward_text"] != nil) {
                
                result.reward_text = [mission objectForKey:@"reward_text"];
            }
            if ([mission objectForKey:@"reward"] != nil) {
                
                result.reward = [mission objectForKey:@"reward"];
            }
            if ([mission objectForKey:@"share_msg"] != nil) {
                
                result.share_msg = [mission objectForKey:@"share_msg"];
            }
            if ([mission objectForKey:@"tasks_count"] != nil) {
                
                result.tasks_count = [mission objectForKey:@"tasks_count"];
            }
            if ([mission objectForKey:@"rules"] != nil) {
                
                result.rules = [MissionRules initWithJSONData:[mission objectForKey:@"rules"]];
            }
            if ([mission objectForKey:@"tasks_types"] != nil) {
                
                result.tasks_types = [TasksTypes initWithJSONData:[mission objectForKey:@"tasks_types"]];
            }
            if ([mission objectForKey:@"type"] != nil) {
                
                result.type = [mission objectForKey:@"type"];
            }
        }
    }
    
    return  result;
}


@end
