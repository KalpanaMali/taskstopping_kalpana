//
//  AllTasks.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AllTasks : NSObject


@property(nonatomic,retain) NSString * user_mission_id;
@property(nonatomic,retain) NSString * pre_expiry_notification;
@property(nonatomic,retain) NSString * validation_code;
@property(nonatomic,retain) NSString * logic_code;
@property(nonatomic,retain) NSArray *  tasks;
@property(nonatomic,retain) NSString * expiry_message;
@property(nonatomic,retain) NSString * completion_time;
@property(nonatomic,retain) NSString * time;
@property(nonatomic,retain) NSString * logic;
@property(nonatomic,retain) NSString * start_range;
@property(nonatomic,retain) NSString * photo_out_of_range_alert;
@property(nonatomic,retain) NSArray *  visible_tasks;
@property(nonatomic,retain) NSString * syncing_error_message;
@property(nonatomic,retain) NSString * logic_dialog;
@property(nonatomic,retain) NSString * start_out_of_range_alert;
@property(nonatomic,retain) NSString * pre_expiry_message;
@property(nonatomic,retain) NSString * connection_error_message;





+ (AllTasks*)initWithJSONData:(NSDictionary *) questionsDict;
@end
