//
//  AllTasks.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "AllTasks.h"
#import "Question.h"

@implementation AllTasks
@synthesize  user_mission_id;
@synthesize  pre_expiry_notification;
@synthesize  validation_code;
@synthesize  logic_code;
@synthesize  tasks;
@synthesize  expiry_message;
@synthesize  completion_time;
@synthesize  time;
@synthesize  logic;
@synthesize  start_range;
@synthesize  photo_out_of_range_alert;
@synthesize  visible_tasks;
@synthesize  syncing_error_message;
@synthesize  logic_dialog;
@synthesize  start_out_of_range_alert;
@synthesize  pre_expiry_message;
@synthesize  connection_error_message;


-(id)init
{
    
    self=[super init];
    
    if (self)
    {
        user_mission_id = @"";
        pre_expiry_notification = @"";
        validation_code = @"";
        logic_code = @"";
        tasks = nil;
        expiry_message = @"";
        completion_time = @"";
        time = @"";
        logic = @"";
        start_range = @"";
        photo_out_of_range_alert = @"";
        visible_tasks = nil;
        syncing_error_message = @"";
        logic_dialog = @"";
        start_out_of_range_alert = @"";
        pre_expiry_message = @"";
        connection_error_message = @"";
    }
    return self;
}


+ (AllTasks*)initWithJSONData:(NSDictionary *) questionsDict
{
    
    
    AllTasks *result = [[AllTasks alloc] init];
    if ([questionsDict objectForKey:@"message"] != nil)
    {
    
    NSDictionary *resultDict = [questionsDict objectForKey:@"message"];
    
    
    if ([resultDict objectForKey:@"user_mission_id"] != nil) {
        result.user_mission_id = [resultDict objectForKey:@"user_mission_id"];
    }
    if ([resultDict objectForKey:@"pre_expiry_notification"] != nil) {
        
        result.pre_expiry_notification = [resultDict objectForKey:@"pre_expiry_notification"];
    }
    if ([resultDict objectForKey:@"validation_code"] != nil) {
        
        result.validation_code = [resultDict objectForKey:@"validation_code"];
    }
    if ([resultDict objectForKey:@"logic_code"] != nil) {
        
        result.logic_code = [resultDict objectForKey:@"logic_code"];
    }
    if ([resultDict objectForKey:@"tasks"] != nil) {
        
        result.tasks = [Question initWithJSONData:[resultDict objectForKey:@"tasks"]];
    }
    if ([resultDict objectForKey:@"expiry_message"] != nil) {
        
        result.expiry_message = [resultDict objectForKey:@"expiry_message"];
    }
    if ([resultDict objectForKey:@"completion_time"] != nil) {
        
        result.completion_time = [resultDict objectForKey:@"completion_time"];
    }
    if ([resultDict objectForKey:@"time"] != nil) {
        
        result.time = [resultDict objectForKey:@"time"];
    }
    if ([resultDict objectForKey:@"logic"] != nil) {
        
        result.logic = [resultDict objectForKey:@"logic"];
    }
    if ([resultDict objectForKey:@"start_range"] != nil) {
        
        result.start_range = [resultDict objectForKey:@"start_range"];
    }
    if ([resultDict objectForKey:@"photo_out_of_range_alert"] != nil) {
        
        result.photo_out_of_range_alert = [resultDict objectForKey:@"photo_out_of_range_alert"];
    }
    if ([resultDict objectForKey:@"visible_tasks"] != nil) {
        
        result.visible_tasks = [resultDict objectForKey:@"visible_tasks"];
    }
    if ([resultDict objectForKey:@"syncing_error_message"] != nil) {
        
        result.syncing_error_message = [resultDict objectForKey:@"syncing_error_message"];
    }
    if ([resultDict objectForKey:@"logic_dialog"] != nil) {
        
        result.logic_dialog = [resultDict objectForKey:@"logic_dialog"];
    }
    if ([resultDict objectForKey:@"start_out_of_range_alert"] != nil) {
        
        result.start_out_of_range_alert = [resultDict objectForKey:@"start_out_of_range_alert"];
    }
    if ([resultDict objectForKey:@"pre_expiry_message"] != nil) {
        
        result.pre_expiry_message = [resultDict objectForKey:@"pre_expiry_message"];
    }
    if ([resultDict objectForKey:@"connection_error_message"] != nil) {
        
        result.connection_error_message = [resultDict objectForKey:@"connection_error_message"];
    }
    }
        
    return  result;
}



@end
