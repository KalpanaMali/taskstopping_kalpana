//
//  MissionRules.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "MissionRules.h"

@implementation MissionRules
@synthesize desc;
@synthesize icon;
@synthesize locked;
@synthesize notification_time;
@synthesize text;


-(id)init
{
    
    self=[super init];
    
    if (self)
    {
        desc                =@"";
        icon                =@"";
        locked              =@"";
        notification_time   =@"";
        text                =@"";
    }
    return self;
}


+ (NSMutableArray*)initWithJSONData:(NSArray *) rulesArr
{
    NSMutableArray *newArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < [rulesArr count]; i++) {
        
        NSDictionary *resultDict = [rulesArr objectAtIndex:i];
        
        MissionRules *result = [[MissionRules alloc] init];
        
        if ([resultDict objectForKey:@"desc"] != nil) {
            result.desc = [resultDict objectForKey:@"desc"];
        }
        if ([resultDict objectForKey:@"icon"] != nil) {
            
            result.icon = [resultDict objectForKey:@"icon"];
        }
        if ([resultDict objectForKey:@"locked"] != nil) {
            if ( [resultDict objectForKey:@"locked"] == [NSNull null]) {
                
            }
            else{
                result.locked = [resultDict objectForKey:@"locked"];
            }
        }
        if ([resultDict objectForKey:@"notification_time"] != nil) {
            
            result.notification_time = [resultDict objectForKey:@"notification_time"];
        }
        if ([resultDict objectForKey:@"text"] != nil) {
            
            result.text = [resultDict objectForKey:@"text"];
        }
      
        
        [newArray addObject:result];
    }
    return  [NSMutableArray arrayWithArray:newArray];
}


@end
