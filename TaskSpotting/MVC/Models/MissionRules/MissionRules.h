//
//  MissionRules.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MissionRules : NSObject


@property(nonatomic,retain) NSString *desc;
@property(nonatomic,retain) NSString *icon;
@property(nonatomic,retain) NSString *locked;
@property(nonatomic,retain) NSString *notification_time;
@property(nonatomic,retain) NSString *text;



+ (NSMutableArray*)initWithJSONData:(NSArray *) rulesArr;


@end
