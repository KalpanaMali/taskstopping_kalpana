//
//  TaskDetail.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TaskDetail : NSObject


@property(nonatomic,retain) NSString *interstitial;
@property(nonatomic,retain) NSMutableArray *options;



+ (TaskDetail*)initWithJSONData:(NSDictionary *) tasksDetailDict;

@end
