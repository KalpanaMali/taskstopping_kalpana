//
//  TaskDetail.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "TaskDetail.h"
#import "QuestionOption.h"


@implementation TaskDetail
@synthesize interstitial;
@synthesize options;


-(id)init
{
    
    self=[super init];
    
    if (self)
    {
        interstitial                = @"";
        options                     = nil;
    }
    return self;
}


+ (TaskDetail*)initWithJSONData:(NSDictionary *) tasksDetailDict
{
        TaskDetail *result = [[TaskDetail alloc] init];
        
        if ([tasksDetailDict objectForKey:@"interstitial"] != nil) {
            result.interstitial = [tasksDetailDict objectForKey:@"interstitial"];
        }
        if ([tasksDetailDict objectForKey:@"options"] != nil) {
            
            result.options = [QuestionOption initWithJSONData:[tasksDetailDict objectForKey:@"options"]];
        }
    
    return  result;
}


@end
