//
//  MissionDetailViewController.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 27/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

#import "AppDelegate.h"

@interface MissionDetailViewController : UIViewController<MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblvMissionDetail;
@property (strong, nonatomic) IBOutlet UIView *mapPinView;
@property (strong, nonatomic) IBOutlet UIView *tableHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (strong, nonatomic) IBOutlet UIView *sectionFooterView;
@property (weak, nonatomic) IBOutlet UILabel *lblProductDetail;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) MKAnnotationView *selectedAnnotationView;
@property (weak, nonatomic) IBOutlet UIImageView *imgvLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblReward;
@property (weak, nonatomic) IBOutlet UILabel *lblPST;

- (IBAction)showAllTasks:(id)sender;

- (MKMapRect)makeMapRectWithAnnotations:(NSArray *)annotations;
-(void) addAnotationOnMapView;
@end

