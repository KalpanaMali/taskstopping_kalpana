//
//  MissionDetailViewController.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 27/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "MissionDetailViewController.h"
#import "MissionDetailCustomCell.h"
#import "MissionDetailCustomCell2.h"
#import "AsynImageDownLoad.h"
#import "TasksTypes.h"
#import "MissionRules.h"
#import "ZSAnnotation.h"
#import "ConnectionManager.h"
#import "MissionDetail.h"
#import "AllTasksViewController.h"
#import "AllTasks.h"
#import "Annotation.h"

@interface MissionDetailViewController (){
    NSMutableArray *arrMissionDetails;
    MissionDetail   *missionDetails;
}

@end

@implementation MissionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrMissionDetails = [[NSMutableArray alloc] init];
    missionDetails = [[MissionDetail alloc] init];

    self.tblvMissionDetail.rowHeight = UITableViewAutomaticDimension;
    self.tblvMissionDetail.estimatedRowHeight = 44;
    
    
    self.tblvMissionDetail.tableHeaderView = _tableHeaderView;
    CGRect size = _tableHeaderView.frame;
    size.size.height = 140;
    _tableHeaderView.frame = size;
    self.tblvMissionDetail.separatorStyle = UITableViewCellSeparatorStyleNone;

}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   // [self addAnotationOnMapView];
    [arrMissionDetails removeAllObjects];
    [self getMissionDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark TableView Delegate


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if(section == 1)
        return 48;
    else
        return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    if(section == 1)
        return self.sectionFooterView;
    else
        return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
        return [arrMissionDetails count];
    else{
        return 1;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
    MissionDetailCustomCell *missionCell = [self.tblvMissionDetail dequeueReusableCellWithIdentifier:@"MissionDetailCustomCell"];
    
    if (!missionCell) {
        
        missionCell = (MissionDetailCustomCell *)[[[NSBundle mainBundle] loadNibNamed:@"MissionDetailCustomCell" owner:self options:nil] lastObject];
        
    }
    
    missionCell.selectionStyle = UITableViewCellSelectionStyleNone;
    MissionRules *rules = [arrMissionDetails objectAtIndex:indexPath.row];
    missionCell.lblRuleText.text = rules.text;
        
    AsynImageDownLoad *asynImageDownLoad = [[AsynImageDownLoad alloc] init];
    [asynImageDownLoad downLoadingAsynImage:missionCell.imgvRuleIcon imagePath:rules.icon backgroundQueue:NULL imageObj:nil];
        AsynImageDownLoad *asynImageDownLoad1 = [[AsynImageDownLoad alloc] init];
     [asynImageDownLoad1 downLoadingAsynImage:missionCell.imgvCheckMark imagePath:rules.locked backgroundQueue:NULL imageObj:nil];
        
    return missionCell;
    }
    else{
        MissionDetailCustomCell2 *missionCell = [self.tblvMissionDetail dequeueReusableCellWithIdentifier:@"MissionDetailCustomCell2"];
        
        if (!missionCell) {
            
            missionCell = (MissionDetailCustomCell2 *)[[[NSBundle mainBundle] loadNibNamed:@"MissionDetailCustomCell2" owner:self options:nil] lastObject];
            
        }
        
        missionCell.selectionStyle = UITableViewCellSelectionStyleNone;
        AsynImageDownLoad *asynImageDownLoad = [[AsynImageDownLoad alloc] init];
        [asynImageDownLoad downLoadingAsynImage:missionCell.imgvType imagePath:[missionDetails.type objectForKey:@"icon"] backgroundQueue:NULL imageObj:nil];
        NSMutableArray *arr = missionDetails.tasks_types;
        TasksTypes *type= nil;
        if([arr count] > 0){
            type = [arr objectAtIndex:0];
            AsynImageDownLoad *asynImageDownLoad = [[AsynImageDownLoad alloc] init];
            [asynImageDownLoad downLoadingAsynImage:missionCell.imgvQuestion imagePath:type.icon backgroundQueue:NULL imageObj:nil];
        }
        
        
        if([arr count] > 1){
            type = [arr objectAtIndex:1];
            AsynImageDownLoad *asynImageDownLoad = [[AsynImageDownLoad alloc] init];
            [asynImageDownLoad downLoadingAsynImage:missionCell.imgvPhoto imagePath:type.icon backgroundQueue:NULL imageObj:nil];
        }
        
        missionCell.lblTaskCount.text = [NSString stringWithFormat:@"%@ TASKS", missionDetails.tasks_count];
        int intSeconds = [missionDetails.mission_time intValue]*60;
        missionCell.lblHours.text = [self getHours:intSeconds];
        if(![missionDetails.expire_date isEqualToString: @""])
            missionCell.lblDays.text = [NSString stringWithFormat:@"%@ Days", [self getDays:missionDetails.expire_date]];


        TasksTypes *tasksType = [[TasksTypes alloc] init];
        if([missionDetails.tasks_types count] > 0){
            tasksType = [missionDetails.tasks_types objectAtIndex:0];
            missionCell.lblQuestionCount.text = [NSString stringWithFormat:@"%@",tasksType.count];

        }
        if([missionDetails.tasks_types count] > 1){
            tasksType = [missionDetails.tasks_types objectAtIndex:1];
            missionCell.lblPhotoCount.text = [NSString stringWithFormat:@"%@",tasksType.count];

        }
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[missionDetails.instructions dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];

        missionCell.lblAboutMission.attributedText = attrStr;
        return missionCell;
        
    }
}


- (IBAction)showAllTasks:(id)sender {
    [self getAllTasks];

    
}
- (NSString *)getDays:(NSString*)expireDate
{
    
    
    
    NSString *dateString = expireDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [[NSDate alloc] init];
    // voila!
    dateFromString = [dateFormatter dateFromString:dateString];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *expiryDate = [dateFormatter stringFromDate:dateFromString];
    NSLog(@"%@", expiryDate);
    
    
    
    
    NSDateFormatter *formatter;
    NSString        *currentDate;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    currentDate = [formatter stringFromDate:[NSDate date]];

    NSString *start = currentDate;
    NSString *end = expiryDate;

    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];

    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:NSCalendarWrapComponents];
    return [NSString stringWithFormat:@"%ld", (long)components.day ];
}
- (NSString *)getHours:(int)totalSeconds
{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    if(minutes == 0){
        return [NSString stringWithFormat:@"%d Hours",hours];
    }
    else{
        return [NSString stringWithFormat:@"%d:%02d",hours, minutes];
    }
}

#pragma mark - MapKit

- (MKMapRect)makeMapRectWithAnnotations:(NSArray *)annotations {
    
    MKMapRect flyTo = MKMapRectNull;
    for (id <MKAnnotation> annotation in annotations) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        } else {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
    
    return flyTo;
    
}
/*

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    static NSString* AnnotationIdentifier = @"Annotation";
    
        // Try to dequeue an existing pin view first.
        MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
//    pinv
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];
            
            AsynImageDownLoad *asynImageDownLoad = [[AsynImageDownLoad alloc] init];
            [asynImageDownLoad downLoadingAsynImage:self.imgvLogo imagePath:missionDetails.logo backgroundQueue:NULL imageObj:nil];
            CGRect size = self.mapPinView.frame;
            size.size.height = 170;
            self.mapPinView.frame = size;
            self.lblProductName.text = missionDetails.name;
            self.lblProductDetail.text = missionDetails.name;
            self.lblReward.text = [missionDetails.reward objectForKey:@"value"];
            self.lblPST.text = [missionDetails.reward objectForKey:@"PTS"];
            pinView.centerOffset = CGPointMake(-150, -self.mapView.frame.size.height / 2);
//            AsynImageDownLoad *asynImageDownLoad1 = [[AsynImageDownLoad alloc] init];
//            [asynImageDownLoad1 downLoadingAsynImage:pinView imagePath:[missionDetails.type objectForKey:@"icon"] backgroundQueue:NULL imageObj:nil];
            pinView.image = [UIImage imageNamed:@"home"];

         //   [pinView addSubview:self.mapPinView];
            pinView.canShowCallout = YES;

        }
        else {
            pinView.annotation = annotation;
            pinView.canShowCallout = true;
            // show pin annotation
           

        }
        return pinView;

}
*/


-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    Annotation *pinView = nil; //create MKAnnotationView Property
    
    static NSString *defaultPinID = @"com.invasivecode.pin"; //Get the ID to change the pin
    pinView = (Annotation *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID]; //Setting custom MKAnnotationView to the ID
    if ( pinView == nil )
        pinView = [[Annotation alloc]
                   initWithAnnotation:annotation reuseIdentifier:defaultPinID]; // init pinView with ID
     CGRect size = self.mapPinView.frame;
     size.size.height = 150;
    size.size.width = 280;
     self.mapPinView.frame = size;
    [pinView addSubview:self.mapPinView];
    addSubview:self.mapPinView.center = CGPointMake(self.mapPinView.bounds.size.width*0.1f, -self.mapPinView.bounds.size.height*0.5f);
    
    
    
    AsynImageDownLoad *asynImageDownLoad = [[AsynImageDownLoad alloc] init];
    [asynImageDownLoad downLoadingAsynImage:self.imgvLogo imagePath:missionDetails.logo backgroundQueue:NULL imageObj:nil];
    self.lblProductName.text = missionDetails.name;
    self.lblProductDetail.text = missionDetails.name;
    NSLog(@"%@%@", [missionDetails.reward objectForKey:@"value"], missionDetails.reward_text);
    self.lblReward.text = [missionDetails.reward objectForKey:@"value"];
    self.lblPST.text = missionDetails.reward_text;
   
    pinView.image = [UIImage imageNamed:@"home"];
    
    
    
    
    return pinView;
}


-(void)setLocation:(CLLocationCoordinate2D)location inBottomCenterOfMapView:(MKMapView*)mapView
{
    //Get the region (with the location centered) and the center point of that region
    MKCoordinateRegion oldRegion = [mapView regionThatFits:MKCoordinateRegionMakeWithDistance(location, 6000, 6000)];
    CLLocationCoordinate2D centerPointOfOldRegion = oldRegion.center;
    
    //Create a new center point (I added a quarter of oldRegion's latitudinal span)
    CLLocationCoordinate2D centerPointOfNewRegion = CLLocationCoordinate2DMake(centerPointOfOldRegion.latitude + oldRegion.span.latitudeDelta/4.0, centerPointOfOldRegion.longitude);
    
    //Create a new region with the new center point (same span as oldRegion)
    MKCoordinateRegion newRegion = MKCoordinateRegionMake(centerPointOfNewRegion, oldRegion.span);
    
    //Set the mapView's region
    [self.mapView setRegion:newRegion animated:YES];
}

-(void) addAnotationOnMapView {
    // Array
    NSMutableArray *annotationArray = [[NSMutableArray alloc] init];
    ZSAnnotation *annotation = [[ZSAnnotation alloc] init];
    
    
    NSArray *arrCordinate = [missionDetails.location_coordinates componentsSeparatedByString:@","];
    double latitude ;
    double longitude;
    if([arrCordinate count] > 1){
        latitude = [[arrCordinate objectAtIndex:0] doubleValue];
        longitude = [[arrCordinate objectAtIndex:1] doubleValue];
    }
    annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    [annotationArray addObject:annotation];
    
    // Center map
    self.mapView.visibleMapRect = [self makeMapRectWithAnnotations:annotationArray];
    
   
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    coordinate.latitude -= self.mapView.region.span.latitudeDelta * 0.10;
    [self.mapView setCenterCoordinate:coordinate animated:YES];
    
    
    MKCoordinateRegion viewRegion =
    MKCoordinateRegionMakeWithDistance
    (coordinate, 6000, 6000);
    viewRegion = [self.mapView regionThatFits:viewRegion];
   // [self.mapView setRegion:viewRegion animated:YES];
    [self setLocation:coordinate inBottomCenterOfMapView:self.mapView];
    self.mapView.zoomEnabled = NO;
    self.mapView.scrollEnabled = NO;
    self.mapView.userInteractionEnabled = YES;
   // [self.mapView selectAnnotation:[[self.mapView annotations] lastObject] animated:YES];
    // Add to map
    [self.mapView addAnnotations:annotationArray];

}

#pragma mark - Server Call

-(void)getMissionDetails {
    [((AppDelegate*)TaskStoppingAppDelegate) showProgressIndicatorOn:self withMessage:@"Please wait"];
    NSMutableDictionary *params = nil;
    
    NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"application/x-www-form-urlencoded",@"Content-Type", nil];
    
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:headers withServiceName:@"https://taskspotting.com/api/30a9fc6214b12ad97c78e9409d370e0369ba1c2a7e44e23b27a8c15af95136edaCz0IkNWRq/2.7/25.047664299999997,55.1817407/missions/1165/A37o5yhQI6kbo63X9T5i2Ed8cCMA1nQO0kdJaYOWFTrV7lHcfZipqvtFrqBbaDu342MxfjmjnmUgP0wuCP1SIKzvzK7ZXRBV9SALsDdNa4esEWpRUHLYhGJy8xGewNlAA/0" withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSLog(@"responseObject %@",responseObject);
         if ([responseObject isKindOfClass:[NSDictionary class]])
         {
             if ([operation.response statusCode]  == 203){
             //    [CommonFunctions showAlert:[operation.responseObject objectForKey:@"response"]];
                 [((AppDelegate*)TaskStoppingAppDelegate) hideProgressIndicatorOn:self];
                 
                 return;
             }
             NSDictionary *responseDict = (NSDictionary*) responseObject;
             
             if (responseDict !=nil)
             {
                 if([operation.response statusCode]  == 200){
                     MissionDetail   *misDetails =[MissionDetail initWithJSONData:responseDict];
                     missionDetails = misDetails;
                     if(missionDetails != nil){
                         [arrMissionDetails addObjectsFromArray:missionDetails.rules];
                         
                         [self.tblvMissionDetail reloadData];
                         [self addAnotationOnMapView];
                     }
                 }
             }
         }
         [self.navigationController.navigationBar setUserInteractionEnabled:YES];
         [((AppDelegate*)TaskStoppingAppDelegate) hideProgressIndicatorOn:self];
         
     }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          NSLog(@"%ld",(long)[operation.response statusCode]);
                                          if([operation.response statusCode]  == 401 ){
                                              
                                              //  NSArray *viewControllers = [[self navigationController] viewControllers];
                                              //                                              for( int i=0;i<[viewControllers count];i++){
                                              //                                                  id obj=[viewControllers objectAtIndex:i];
                                              //                                                  if([obj isKindOfClass:[PhoneNumberVC class]]){
                                              //                                                      [CommonFunctions showAlert:[operation.responseObject objectForKey:@"response"]];
                                              //                                                      [[self navigationController] popToViewController:obj animated:YES];
                                              //                                                      return;
                                              //                                                  }
                                              //                                              }
                                          }
                                          else if ([operation.response statusCode]  == 500){
                                              
//                                              [CommonFunctions showAlert:@"Some problem occured. Please try again later."];
                                          }
                                          else if ([operation.response statusCode]  == 502 ){
//                                              [CommonFunctions showAlert:[operation.responseObject objectForKey:@"Could not connect to server. Please check your proxy server/ firewall settings."]];
//                                              
                                          }
                                          else if ([operation.response statusCode]  == 400){
                                              
                                          }
                                          
                                          else if ([operation.response statusCode]  == 0){
//                                              [CommonFunctions showAlert:@"Request timeout.Please try again."];
                                          }
                                          else if ([operation.response statusCode]  == 203){
//                                              [CommonFunctions showAlert:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          [self.navigationController.navigationBar setUserInteractionEnabled:YES];
                                          [((AppDelegate*)TaskStoppingAppDelegate) hideProgressIndicatorOn:self];
                                      }];
    
}


-(void)getAllTasks {
    [((AppDelegate*)TaskStoppingAppDelegate) showProgressIndicatorOn:self withMessage:@"Please wait"];

    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   
                                   @"start", @"op", nil];
    
    NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"application/x-www-form-urlencoded",@"Content-Type", nil];
    
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:headers withServiceName:@"https://taskspotting.com/api/30a9fc6214b12ad97c78e9409d370e0369ba1c2a7e44e23b27a8c15af95136edaCz0IkNWRq/2.7/25.047664299999997,55.1817407/missions/1165/A37o5yhQI6kbo63X9T5i2Ed8cCMA1nQO0kdJaYOWFTrV7lHcfZipqvtFrqBbaDu342MxfjmjnmUgP0wuCP1SIKzvzK7ZXRBV9SALsDdNa4esEWpRUHLYhGJy8xGewNlAA/0" withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSLog(@"responseObject %@",responseObject);
         if ([responseObject isKindOfClass:[NSDictionary class]])
         {
             if ([operation.response statusCode]  == 203){
                 //    [CommonFunctions showAlert:[operation.responseObject objectForKey:@"response"]];
                 [((AppDelegate*)TaskStoppingAppDelegate) hideProgressIndicatorOn:self];
                 
                 return;
             }
             NSDictionary *responseDict = (NSDictionary*) responseObject;
             
             if (responseDict !=nil)
             {
                 if([operation.response statusCode]  == 200){
                     AllTasks   *allTask =[AllTasks initWithJSONData:responseDict];
                     
                     AllTasksViewController *allTasksViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AllTasksViewController"];
                     allTasksViewController.allTasks = allTask;
                     allTasksViewController.missionDetails = missionDetails;
                     UINavigationController *navigationController =
                     [[UINavigationController alloc] initWithRootViewController:allTasksViewController];
                     
                     [self presentViewController:navigationController
                                        animated:YES
                                      completion:^{
                                          
                                          
                                      }];
                 }
             }
         }
         [self.navigationController.navigationBar setUserInteractionEnabled:YES];
         [((AppDelegate*)TaskStoppingAppDelegate) hideProgressIndicatorOn:self];
         
     }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          NSLog(@"%ld",(long)[operation.response statusCode]);
                                          if([operation.response statusCode]  == 401 ){
                                              
                                              //  NSArray *viewControllers = [[self navigationController] viewControllers];
                                              //                                              for( int i=0;i<[viewControllers count];i++){
                                              //                                                  id obj=[viewControllers objectAtIndex:i];
                                              //                                                  if([obj isKindOfClass:[PhoneNumberVC class]]){
                                              //                                                      [CommonFunctions showAlert:[operation.responseObject objectForKey:@"response"]];
                                              //                                                      [[self navigationController] popToViewController:obj animated:YES];
                                              //                                                      return;
                                              //                                                  }
                                              //                                              }
                                          }
                                          else if ([operation.response statusCode]  == 500){
                                              
                                              //                                              [CommonFunctions showAlert:@"Some problem occured. Please try again later."];
                                          }
                                          else if ([operation.response statusCode]  == 502 ){
                                              //                                              [CommonFunctions showAlert:[operation.responseObject objectForKey:@"Could not connect to server. Please check your proxy server/ firewall settings."]];
                                              //
                                          }
                                          else if ([operation.response statusCode]  == 400){
                                              
                                          }
                                          
                                          else if ([operation.response statusCode]  == 0){
                                              //                                              [CommonFunctions showAlert:@"Request timeout.Please try again."];
                                          }
                                          else if ([operation.response statusCode]  == 203){
                                              //                                              [CommonFunctions showAlert:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          [self.navigationController.navigationBar setUserInteractionEnabled:YES];
                                          [((AppDelegate*)TaskStoppingAppDelegate) hideProgressIndicatorOn:self];
                                      }];
    
}

@end
