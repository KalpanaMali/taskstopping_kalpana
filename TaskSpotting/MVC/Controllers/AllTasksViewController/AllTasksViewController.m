//
//  AllTasksViewController.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//


#import "MissionDetailCustomCell.h"
#import "AllTasksViewController.h"
#import "Question.h"
#import "AsynImageDownLoad.h"
#import "SingleTaskViewController.h"


@interface AllTasksViewController (){
    NSMutableArray *arrTasks;
    NSTimer *timer;
    int currMinute;
    int currSeconds;

}

@end

@implementation AllTasksViewController
@synthesize allTasks, missionDetails;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    if([allTasks.tasks count] > 0){
        arrTasks = [[NSMutableArray alloc] init];
        for (int i = 0; i < [allTasks.tasks count]; i++) {
            Question *question = [allTasks.tasks objectAtIndex:i];
            
            for (int j = 0; j < [allTasks.visible_tasks count]; j++) {
                NSNumber *taskId = [allTasks.visible_tasks objectAtIndex:j];
                if([question.task_id integerValue]== [taskId integerValue])
                    [arrTasks addObject:question];
            }
            
        }
    }
    self.lblTaskCount.text = [NSString stringWithFormat:@"%lu TASKS", (unsigned long)[arrTasks count] ];
    self.lblProductName.text = missionDetails.name;
    self.navigationController.navigationBar.hidden = YES;
   // arrVisibleTasks = arrTasks;
    currMinute = [self.missionDetails.mission_time intValue];
    currSeconds= 00;
    [self.lblTimer setText:[NSString stringWithFormat:@"%d M : %d S", currMinute, currSeconds]];
    [self start];
}


-(void)start
{
    timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    
}
-(void)timerFired
{
    if((currMinute>0 || currSeconds>=0) && currMinute>=0)
    {
        if(currSeconds==0)
        {
            currMinute-=1;
            currSeconds=59;
        }
        else if(currSeconds>0)
        {
            currSeconds-=1;
        }
        if(currMinute>-1)
        
        [self.lblTimer setText:[NSString stringWithFormat:@"%d M : %02d S", currMinute, currSeconds]];

    }
    else
    {
        [timer invalidate];
        [self dismissViewControllerAnimated:YES completion:nil];

    }
}





#pragma mark TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SingleTaskViewController *singleTaskViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SingleTaskViewController"];
    singleTaskViewController.allTasks = allTasks;
    singleTaskViewController.missionDetails = missionDetails;
    singleTaskViewController.currentPage = indexPath.row;
    singleTaskViewController.arrTasks = arrTasks;
    [self.navigationController pushViewController:singleTaskViewController animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

#pragma mark TableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrTasks count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MissionDetailCustomCell *missionCell = [self.tblvTasks dequeueReusableCellWithIdentifier:@"MissionDetailCustomCell"];
    
    if (!missionCell) {
        
        missionCell = (MissionDetailCustomCell *)[[[NSBundle mainBundle] loadNibNamed:@"MissionDetailCustomCell" owner:self options:nil] lastObject];
        
    }

    missionCell.selectionStyle = UITableViewCellSelectionStyleNone;
    Question *question = [arrTasks objectAtIndex:indexPath.row];
    NSLog(@"%@",question.answerID);
    if(question.answerID == nil || [question.answerID integerValue] == -1)
    {
        [missionCell.imgvCheckMark setImage:[UIImage imageNamed:@"Right_Arrow"]];

    }
    else{
        [missionCell.imgvCheckMark setImage:[UIImage imageNamed:@"BlueCheckMark"]];
    }
    
    AsynImageDownLoad *asynImageDownLoad = [[AsynImageDownLoad alloc] init];
    [asynImageDownLoad downLoadingAsynImage:missionCell.imgvRuleIcon imagePath:question.icon backgroundQueue:NULL imageObj:nil];

    
    missionCell.lblRuleText.text = question.question;
    missionCell.lblRuleText.adjustsFontSizeToFitWidth = YES;

    
    return missionCell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) updateTasks: (NSMutableArray*) tasks{
    arrTasks = tasks;
    [self.tblvTasks reloadData];
}

@end
