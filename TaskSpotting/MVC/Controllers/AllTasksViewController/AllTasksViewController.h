//
//  AllTasksViewController.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AllTasks.h"
#import "MissionDetail.h"


@interface AllTasksViewController : UIViewController
@property(nonatomic, strong) AllTasks *allTasks;
@property (weak, nonatomic) IBOutlet UITableView *tblvTasks;
@property (weak, nonatomic) IBOutlet UILabel *lblTimer;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskCount;
@property (strong, nonatomic)  MissionDetail   *missionDetails;

- (IBAction)dismissController:(id)sender;
- (void) updateTasks: (NSMutableArray*) tasks;
@end


