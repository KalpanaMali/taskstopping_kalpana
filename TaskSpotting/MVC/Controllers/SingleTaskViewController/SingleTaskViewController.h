//
//  SingleTaskViewController.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AllTasks.h"
#import "SwipeView.h"
#import "MissionDetail.h"
#import "MultiChoiceView.h"




@interface SingleTaskViewController : UIViewController <GetSelectedChoicesDelagate>
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblTimer;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblTaskCount;
@property(nonatomic, assign) NSInteger currentPage;
@property(nonatomic, strong) NSMutableArray *arrTasks;

@property(nonatomic, strong) AllTasks *allTasks;
@property (strong, nonatomic)  MissionDetail   *missionDetails;
- (IBAction)popViewController:(id)sender;
- (IBAction)yesButtonClicked:(id)sender;
- (IBAction)noButtonClicked:(id)sender;
- (IBAction)nextClicked:(id)sender;
- (IBAction)ForwardButtonClicked:(id)sender;
- (IBAction)backwardButtonClicked:(id)sender;

@end

