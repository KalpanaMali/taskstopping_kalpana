//
//  SingleTaskViewController.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 29/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

@import JavaScriptCore;

#import "MissionDetailCustomCell.h"
#import "SingleTaskViewController.h"
#import "Question.h"
#import "AsynImageDownLoad.h"
#import "YesNoView.h"
#import "QuestionOption.h"
#import "NewPath.h"
#import "AllTasksViewController.h"


@interface SingleTaskViewController ()<SwipeViewDataSource, SwipeViewDelegate>{
    NSMutableArray *arrTasks;
    NSMutableArray *arrAllAnswer;
    UIView *currentView;
    Question *currentQuestion;
    NSTimer *timer;
    int currMinute;
    int currSeconds;
    BOOL isQuestionTypeYesNo;
    BOOL isAnswered;
    NSMutableArray *visibleQuestion;
    
}

@property (nonatomic, weak) IBOutlet SwipeView *swipeView;


@end

@implementation SingleTaskViewController
@synthesize allTasks;
@synthesize missionDetails, currentPage, arrTasks;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    currentView = [[UIView alloc] init];
  //  arrTasks = [[NSMutableArray alloc] initWithArray:arrVisibleTasks];
    visibleQuestion = [[NSMutableArray alloc] init];
   //  arrTasks = [[NSMutableArray alloc] initWithObjects:[allTasks.tasks objectAtIndex:1], nil];
    arrAllAnswer = [[NSMutableArray alloc] init];
    currentQuestion = [[Question alloc] init];

    
    
    
    self.lblTaskCount.text = [NSString stringWithFormat:@"%lu TASKS", (unsigned long)[arrTasks count] ];
    self.lblProductName.text = missionDetails.name;
    self.navigationController.navigationBar.hidden = YES;
  //  arrVisibleTasks = arrTasks;
    currMinute = [self.missionDetails.mission_time intValue];
    currSeconds= 00;
    [self.lblTimer setText:[NSString stringWithFormat:@"%d M : %d S", currMinute, currSeconds]];
    self.btnNext.enabled = NO;
    self.btnNext.backgroundColor = [UIColor lightGrayColor];
    [self start];
    _swipeView.pagingEnabled = YES;
    [_swipeView setNumberOfItems:[arrTasks count]];
    [_swipeView scrollToItemAtIndex:currentPage duration:0.01];
    for (int i = 0; i < currentPage; i++) {
        Question *que = [arrTasks objectAtIndex:i];

        [visibleQuestion addObject:que.task_id];
    }
    _swipeView.dataSource = self;
    _swipeView.delegate = self;

}

-(void)start
{
    timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    
}
-(void)timerFired
{
    if((currMinute>0 || currSeconds>=0) && currMinute>=0)
    {
        if(currSeconds==0)
        {
            currMinute-=1;
            currSeconds=59;
        }
        else if(currSeconds>0)
        {
            currSeconds-=1;
        }
        if(currMinute>-1)
            
            [self.lblTimer setText:[NSString stringWithFormat:@"%d M : %02d S", currMinute, currSeconds]];
        
    }
    else
    {
        [timer invalidate];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
}


- (void)getNewPath
{
    // getting a JSContext
    JSContext *context = [JSContext new];
    
    // enable exception handling
    [context setExceptionHandler:^(JSContext *context, JSValue *value) {
        NSLog(@"%@", value);
    }];
    
    // defining a JavaScript function
    NSString *jsFunctionText = allTasks.logic_code;
    [context evaluateScript:jsFunctionText];
    
    // calling a JavaScript function
    JSValue *jsFunction = context[@"logic"];
    JSValue *value = [jsFunction callWithArguments:@[ arrAllAnswer ]];
    
    
    NSMutableString *javascriptString = [NSMutableString stringWithString:[value toObject]];
    
    value = [context evaluateScript:javascriptString];
    NSArray *arrNewPath = [value toArray];
    NSLog(@"javascriptArray returned = %@", arrNewPath);

    NSDictionary *dict = nil;
    
    if(arrNewPath.count > 0){
    
    dict = [arrNewPath objectAtIndex:0];

    }
   
    if([[dict objectForKey:@"id"] integerValue] == -1){

     //   [_swipeView scrollToItemAtIndex:_swipeView.currentItemIndex+1 duration:1];

    }
    else{
        [arrTasks removeAllObjects];
        
        NSInteger index = [visibleQuestion indexOfObject:currentQuestion.task_id];
        
        NSRange r;
        r.location = index+1;
        r.length = visibleQuestion.count  - (index+1);
        
        [visibleQuestion removeObjectsInRange:r];
        
        for (int i = 0 ; i < [allTasks.tasks count]; i++) {
            for (int j = 0 ; j < [visibleQuestion count]; j++) {
                
                Question *que = [allTasks.tasks objectAtIndex:i];
                NSNumber *taskID = [visibleQuestion objectAtIndex:j];
                if([que.task_id integerValue]== [taskID integerValue]){
                    
                    [arrTasks addObject:que];
                    
                }
                
            }
        }
        
        for (int i = 0 ; i < [arrNewPath count]; i++) {
            for (int j = 0 ; j < [allTasks.tasks count]; j++) {
                
                Question *que = [allTasks.tasks objectAtIndex:j];
                NSDictionary *dict = [arrNewPath objectAtIndex:i];
                if([que.task_id integerValue]== [[dict objectForKey:@"id"] integerValue]){
                    
                    [arrTasks addObject:que];
                    break;
                }
                
            }
        }
        
//        for (int i = 0 ; i < [arrTasks count]; i++) {
//            Question *que = [arrTasks objectAtIndex:i];
//            que.queIndex = i;
//
//        }
        NSLog(@"dsfsdf");
    }
    [_swipeView reloadData];
    [_swipeView scrollToItemAtIndex:_swipeView.currentItemIndex+1 duration:1];
}


#pragma mark TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return [arrTasks count];
}


- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    
//    if (view == nil)
//    {
        if([arrTasks count] > 0){
            Question *que = [arrTasks objectAtIndex:index];
           
//            for (int i = 0 ; i < [arrTasks count]; i++) {
//                Question *que1 = [arrTasks objectAtIndex:i];
//                if(que1.queIndex == index){
//                    que = [arrTasks objectAtIndex:i];
//                    //currentPage =_swipeView.sc
//                    NSLog(@"%@", que.question);
//                }
//                
//            }

            
            if(![visibleQuestion containsObject:que.task_id])
                [visibleQuestion addObject:que.task_id];
            if([que.type isEqualToString:@"yes_no"]){
                isQuestionTypeYesNo = YES;

                view = (YesNoView*)[[[NSBundle mainBundle] loadNibNamed:@"YesNoView" owner:self options:nil] lastObject];
                YesNoView *newView = (YesNoView*)view;
                newView.lblQuestion.text = que.question;
                if(que.answerID != nil){
                    NSArray *arr = que.details.options;
                    self.btnNext.enabled = YES;
                    self.btnNext.backgroundColor = [UIColor cyanColor];
                    for (int i = 0; i < arr.count; i++) {
                        QuestionOption *options = [arr objectAtIndex:i];
                        if([options.option_id integerValue] == [que.answerID integerValue]){
                            if([options.value isEqualToString:@"Yes"]){
                                [newView.btnYes setBackgroundColor:[UIColor cyanColor]];
                            }
                            else{
                                [newView.btnNo setBackgroundColor:[UIColor cyanColor]];
                            }
                        }
                    }
                }
                else{
                    if(que.answerID == nil)
                        que.answerID = [NSNumber numberWithInt:-1];
                }
                currentQuestion = que;
                currentView = view;
            }
            else if([que.type isEqualToString:@"product_avail"]){
                isQuestionTypeYesNo = NO;

                view = (MultiChoiceView*)[[[NSBundle mainBundle] loadNibNamed:@"MultiChoiceView" owner:self options:nil] lastObject];
                MultiChoiceView *newView = (MultiChoiceView*)view;
                newView.delegate = self;
                newView.lblQuestion.text = que.question;
               
                
                if(que.multipleAnswerID.count > 0){
                    newView.arrSelectedChoices = [[NSMutableArray alloc] init];

                    NSMutableArray *arr = que.details.options;
                    newView.arrChoices = que.details.options;

                    for (int i = 0; i < arr.count; i++) {
                        
                        for (int j = 0; j < que.multipleAnswerID.count; j++) {

                            QuestionOption *options = [arr objectAtIndex:i];
                            QuestionOption *options1 = [que.multipleAnswerID objectAtIndex:j];
                            if([options.option_id integerValue] == [options1.option_id integerValue]){
                                options.isSelected = YES;
                                [newView.arrSelectedChoices addObject:options];
                                [newView.arrChoices replaceObjectAtIndex:i withObject:options];

                            }
                            
                        }

                    }
                }
                else{
                    newView.arrSelectedChoices = [[NSMutableArray alloc] init];
                    newView.arrChoices = que.details.options;
                }
                if(que.answerID == nil)
                    que.answerID = [NSNumber numberWithInt:-1];
                
                currentQuestion = que;
                currentView = view;

            }
            else{
                isQuestionTypeYesNo = NO;
                view = (YesNoView*)[[[NSBundle mainBundle] loadNibNamed:@"YesNoView" owner:self options:nil] lastObject];
                YesNoView *newView = (YesNoView*)view;
                newView.lblQuestion.text = que.question;
                currentQuestion = que;
                currentView = view;
                newView.btnNo.hidden = YES;
                newView.btnYes.hidden = YES;
                newView.lblQueType.hidden = YES;
                if(que.answerID == nil)
                    que.answerID = [NSNumber numberWithInt:-1];
            }
        }
 //   }
   /* else{
        
        if([arrTasks count] > 0){
            Question *que = [arrTasks objectAtIndex:index];
            if([que.type isEqualToString:@"yes_no"]){

                YesNoView *newView = (YesNoView*)view;
                newView.lblQuestion.text = que.question;
                currentQuestion = que;
                currentView = view;

            }
            else if([que.type isEqualToString:@"product_avail"]){
                MultiChoiceView *newView = (MultiChoiceView*)view;
                newView.lblQuestion.text = que.question;
                newView.arrChoices = que.details.options;
                newView.arrSelectedChoices = [[NSMutableArray alloc] init];
                currentQuestion = que;
                currentView = view;
            }
            else{
                YesNoView *newView = (YesNoView*)view;
                newView.lblQuestion.text = que.question;
                currentQuestion = que;
                currentView = view;
                newView.btnNo.hidden = YES;
                newView.btnYes.hidden = YES;
                
            }
        }
    }*/
    return view;
}

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return self.swipeView.bounds.size;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)popViewController:(id)sender {
    
      NSArray *viewControllers = [[self navigationController] viewControllers];
      for( int i=0;i<[viewControllers count];i++){
          id obj=[viewControllers objectAtIndex:i];
          if([obj isKindOfClass:[AllTasksViewController class]]){
              AllTasksViewController *tasksVC = (AllTasksViewController*)obj;
              [tasksVC updateTasks:arrTasks];
              [self.navigationController popViewControllerAnimated:YES];
              return;
          }
      }
    

}
#pragma mark MultiChoiceDelagate

- (void)selectedChoices:(NSMutableArray *)arrChoices{
    if([arrChoices count] > 0)
    {
        self.btnNext.backgroundColor = [UIColor cyanColor];
      //  isAnswered = YES;
        QuestionOption *que = [arrChoices objectAtIndex:0];
        currentQuestion.answerID = que.option_id;
        currentQuestion.multipleAnswerID = [[NSMutableArray alloc] initWithArray:arrChoices];
        //currentQuestion.multipleAnswerID = arrChoices;
    }
    else{
        
        self.btnNext.backgroundColor = [UIColor lightGrayColor];
    //    isAnswered = NO;

    }
}




#pragma mark Actions


- (IBAction)ForwardButtonClicked:(id)sender {
    [_swipeView scrollToItemAtIndex:_swipeView.currentItemIndex+1 duration:1];

}

- (IBAction)backwardButtonClicked:(id)sender {
    [_swipeView scrollToItemAtIndex:_swipeView.currentItemIndex-1 duration:1];

}

- (IBAction)yesButtonClicked:(id)sender{
    isAnswered = YES;
    isQuestionTypeYesNo = YES;
    YesNoView *newView = (YesNoView*)currentView;
    newView.btnYes.backgroundColor = [UIColor cyanColor];
    newView.btnNo.backgroundColor = [UIColor whiteColor];
    NSArray *arrAnswers = currentQuestion.details.options;
    if([arrAnswers count] > 0){
        QuestionOption *answer = [arrAnswers objectAtIndex:0];
        if([answer.value isEqualToString:@"Yes"]){
            currentQuestion.answerID = answer.option_id;
        }
    }
    self.btnNext.enabled = YES;
    self.btnNext.backgroundColor = [UIColor cyanColor];
}
- (IBAction)noButtonClicked:(id)sender{
    isAnswered = YES;
    isQuestionTypeYesNo = YES;
    YesNoView *newView = (YesNoView*)currentView;
    newView.btnYes.backgroundColor = [UIColor whiteColor];
    newView.btnNo.backgroundColor = [UIColor cyanColor];
    
    NSArray *arrAnswers = currentQuestion.details.options;
    if([arrAnswers count] > 1){
        QuestionOption *answer = [arrAnswers objectAtIndex:1];
        if([answer.value isEqualToString:@"No"]){
            currentQuestion.answerID = answer.option_id;
        }
    }
    self.btnNext.enabled = YES;
    self.btnNext.backgroundColor = [UIColor cyanColor];
}

- (IBAction)nextClicked:(id)sender {
    NSLog(@"%lu %lu",(unsigned long)visibleQuestion.count, (unsigned long)_swipeView.indexesForVisibleItems.count);
//    if([q.task_id integerValue] == [currentQuestion.task_id integerValue]){
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else{
    if(isAnswered == YES){
        isAnswered = NO;
        self.btnNext.backgroundColor = [UIColor lightGrayColor];
        if(arrAllAnswer.count == 0){
            NSMutableArray *anwser = [[NSMutableArray alloc] init];
            [anwser addObject:currentQuestion.task_id];
            [anwser addObject:currentQuestion.answerID];
            [arrAllAnswer addObject:anwser];
        }
        else{
            for (int i = 0; i < arrAllAnswer.count; i++) {
                NSArray *arr = [arrAllAnswer objectAtIndex:i];
                if([arr objectAtIndex:0] == currentQuestion.task_id){
                    NSMutableArray *anwser = [[NSMutableArray alloc] init];
                    [anwser addObject:currentQuestion.task_id];
                    [anwser addObject:currentQuestion.answerID];
                    for (int j = 0; j < arrAllAnswer.count; j++) {
                        NSMutableArray *oldAnswer = [arrAllAnswer objectAtIndex:j];
                        if([oldAnswer objectAtIndex:0] == currentQuestion.task_id){
                            [arrAllAnswer replaceObjectAtIndex:j withObject:anwser];
                        }
                    }
                }
                else{
                    NSMutableArray *anwser = [[NSMutableArray alloc] init];
                    [anwser addObject:currentQuestion.task_id];
                    [anwser addObject:currentQuestion.answerID];
                    [arrAllAnswer addObject:anwser];
                }
            }
        }
        if(isQuestionTypeYesNo){
            isQuestionTypeYesNo = NO;
            [self getNewPath];
        }
        else{
            [_swipeView scrollToItemAtIndex:_swipeView.currentItemIndex+1 duration:1];
        }

    }
    else{
        [_swipeView scrollToItemAtIndex:_swipeView.currentItemIndex+1 duration:1];
    }
    Question *q = [arrTasks objectAtIndex:arrTasks.count-1];

    if([q.task_id integerValue] == [currentQuestion.task_id integerValue]){
        NSArray *viewControllers = [[self navigationController] viewControllers];
        for( int i=0;i<[viewControllers count];i++){
            id obj=[viewControllers objectAtIndex:i];
            if([obj isKindOfClass:[AllTasksViewController class]]){
                AllTasksViewController *tasksVC = (AllTasksViewController*)obj;
                [tasksVC updateTasks:arrTasks];
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
        }
    }
   // }
}


@end
