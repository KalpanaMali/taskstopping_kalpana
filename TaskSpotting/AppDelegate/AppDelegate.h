//
//  AppDelegate.h
//  TaskSpotting
//
//  Created by KalpanaShikhar on 27/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "MissionDetailViewController.h"
#import "MBProgressHUD.h"



#define TaskStoppingAppDelegate (AppDelegate *)[[UIApplication sharedApplication] delegate]




@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, assign)    double          userLatitude ;
@property (nonatomic, assign)    double          userLongitude;
@property (nonatomic, strong)  MBProgressHUD    *loading;

- (void)startLocationManager;
- (void)showProgressIndicatorOn:(UIViewController*)viewController withMessage:(NSString*)message;
- (void)hideProgressIndicatorOn:(UIViewController*)viewController;
@end

