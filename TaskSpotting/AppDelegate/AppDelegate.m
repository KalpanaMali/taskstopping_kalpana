//
//  AppDelegate.m
//  TaskSpotting
//
//  Created by KalpanaShikhar on 27/05/16.
//  Copyright © 2016 Kalpana. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize loading;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
  //  [self startLocationManager];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    self.locationManager = nil;
    [self startLocationManager];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark Loading View Methods

- (void)showProgressIndicatorOn:(UIViewController*)viewController withMessage:(NSString*)message{
    loading = [[MBProgressHUD alloc] initWithView:viewController.view];
    [viewController.view addSubview:loading];
    //    loading.labelText = message;
    [loading show:YES];
}

- (void)hideProgressIndicatorOn:(UIViewController*)viewController {
    
    if (loading){
        [loading hide:YES];
    }
}


#pragma mark Location Delegate
- (void)startLocationManager {
    
    if (self.locationManager == nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = 100.0f;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
    
    [self.locationManager startUpdatingLocation];
}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *newLocation = [locations lastObject];
    CLLocationCoordinate2D coordinate = [newLocation coordinate];
    self.userLatitude = coordinate.latitude;
    self.userLongitude = coordinate.longitude;
    NSLog(@"\n userLatitude %f \n userLongitude %f", self.userLatitude, self.userLongitude);
    
    UINavigationController *navController = (UINavigationController*)self.window.rootViewController;
    if([navController.viewControllers count] > 0){
        MissionDetailViewController *missionController = [navController.viewControllers objectAtIndex:0];
        [missionController addAnotationOnMapView];
    }
}

- (void)locationManager: (CLLocationManager *)manager
       didFailWithError: (NSError *)error
{
    NSLog(@"error%@",error);
    switch([error code])
    {
        case kCLErrorNetwork: // general, network-related error
        {
            
            /*
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Error"
                                          message:@"Please check your network connection or that you are not in airplane mode"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           //Handel no, thanks button
                                           
                                       }];
            
            [alert addAction:noButton];
            
            
            
            self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            self.window.rootViewController = [[UIViewController alloc] init];
            self.window.windowLevel = UIWindowLevelAlert + 1;
            [self.window makeKeyAndVisible];
            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
            */
            }
            break;
        case kCLErrorDenied:{
            
            /*
            
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Error"
                                          message:@"You denied to use current Location. Please on location service from your phone's settings."
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           //Handel no, thanks button
                                           
                                       }];
            
            [alert addAction:noButton];
            
            
            
            self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            self.window.rootViewController = [[UIViewController alloc] init];
            self.window.windowLevel = UIWindowLevelAlert + 1;
            [self.window makeKeyAndVisible];
            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];*/
        }
            break;
        default:
        {
        }
            break;
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusDenied) {
        // permission denied
        NSLog(@"permission denied");
       
    }
    else if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        // permission granted
        NSLog(@"permission granted");
        
    }
}


@end
